#!/usr/bin/env python
from typing import Dict, Callable
import pandas
import argparse
import sys
import time

from . import misc


def trace(node: str, graph: Dict[str, str], cond: Callable[[str], bool]) -> str:
    """ Given a node node in a tree graph, move up the tree until cond is true,
        return final node 
    """
    while(not cond(node)):
        node = graph[node]
    return node


def main(orthogroups_path, gff3_paths):
    start = time.time()
    # args = get_args()
    # load and check data
    df = misc.load_orthos(orthogroups_path)
    sys.stderr.write(f"Loaded Orthogroups : {time.time() - start}\n")
    if(max(df.groupby("Protein").Sample.unique().apply(len)) > 1):
        raise ValueError("Provided protein set contains duplicates between samples. Unable to parse")
    if(len(gff3_paths) != len(df.Sample.unique())):
        raise ValueError("You must provide a gff3  for each sample provided to orthofinder")
    gene_info = {}
    name_to_id = {}
    id_to_parent = {}
    for f in gff3_paths:
        gff = misc.load_gff(f)
        genes = gff[gff.type=="gene"]
        gene_info.update({r.ID: r for _, r in genes.iterrows()})
        name_to_id.update({n: i for n, i in zip(gff.attr_Name, gff.ID)})
        id_to_parent.update({i: p for i, p in zip(gff.ID, gff.Parent)})

    names_and_ids = set(name_to_id) | set(id_to_parent)
    sys.stderr.write(f"Loaded GFFS : {time.time() - start}\n")

    def prefix_search(pname):
        original = pname
        while(pname not in names_and_ids):
            pname = pname[:-1]
            if(len(pname) <= 1):
                raise ValueError(f"{original} can't be found in provided gff3 files")
        return pname

    df["valid_prefix"] = df.Protein.apply(prefix_search)
    prot_to_id = {prot: name_to_id.get(prot, prot) for prot in df["valid_prefix"]}
    prot_to_gene = {
        prot: trace(ID, id_to_parent, lambda x: x in gene_info)
        for prot, ID in prot_to_id.items()
    }
    sys.stderr.write(f"Linked GFFS and Orthogroups : {time.time() - start}\n")

    # update df
    df["Gene"] = df.valid_prefix.apply(prot_to_gene.get)
    info = df.Gene.apply(gene_info.get)
    df["chr"] = info.seqid
    df["start"] = info.start
    df["end"] = info.end
    df["strand"] = info.strand
    df.to_csv(sys.stdout, sep="\t", index=None)

