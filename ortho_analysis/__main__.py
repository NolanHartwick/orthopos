import argparse
from pkg_resources import resource_filename
import os

from . import misc
from . import orthopos
from . import orthoupset


def main():
    parser = argparse.ArgumentParser(prog='A set of tools for analysis of orthogroups')
    subparsers = parser.add_subparsers()

    parser_gos = subparsers.add_parser("gos", help="identifies go terms overrepresented among each orthogroup")
    gos_args(parser_gos)
    parser_gos.set_defaults(which='gos')

    parser_pos = subparsers.add_parser("pos", help="combines genomic positional information with orthogroup infromation")
    pos_args(parser_pos)
    parser_pos.set_defaults(which="pos")

    parser_upset = subparsers.add_parser("upset", help="visualizes orthogroups using upsetplots")
    upset_args(parser_upset)
    parser_upset.set_defaults(which="upset")

    parser_comps = subparsers.add_parser("comp", help="identifies orthogroups that are enlarged in a sample subset")
    comps_args(parser_comps)
    parser_comps.set_defaults(which="comp")
    args = parser.parse_args()
    if(args.which == "gos"):
        misc.orthogos(args.orthogroups, args.gos, args.weights, args.obo, args.filter)
    elif(args.which == "comp"):
        misc.orthocomps(args.orthogroups, args.comparisons, args.sample)
    elif(args.which == "pos"):
        orthopos.main(args.orthogroups, args.gff3)
    elif(args.which == "upset"):
        orthoupset.main(args)
    else:
        print(args)
        print("NI")


def gos_args(parser):
    """ add orthogos related args to subparser parser
    """
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument(
        "gos",
        nargs='+',
        type=lambda s: (s.split("=", 1)[0], s.split("=", 1)[1]),
        help=(
            "key=value pair in which key is a column header from orthogroups "
            "and the value is a path to a two column table in which the first "
            "column contains gene ids and the second column contains a comma "
            "seperated list of go terms. Lines with duplicate gene ids will "
            "be merged prior to further processing"
        )
    )
    parser.add_argument(
        "-w", "--weights",
        nargs=3,
        type=float,
        default=[1,1,1],
        help=(
            "All relationships are scored according to the formula: "
            "(1-P)^A * logit(fc)^B * (1 - max(child_score))^C ."
            "Where A, B, and C are the parameters given to weights. "
            "Basically this score is attempting to reward significant "
            "results with large effect size and little incidnetal mixture "
            "with other effects"
        )
    )
    parser.add_argument(
        "-o", "--obo",
        default=os.path.abspath(resource_filename('ortho_analysis', 'go-basic.obo')),
        help=(
            "an ontology file (obo) which is used for identifying parent/child "
            "relationships during score computation. You probably don't need to "
            "use/change this parameter"
        )
    )
    parser.add_argument(
        "-f", "--filter",
        type=int,
        default=3,
        help="show only the most <filter> significant gos for each OG. Use -1 to disable filtering"
    )

def pos_args(parser):
    """ add orthopos related args to subparser parser
    """
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    # parser.add_argument("-f", "--fasta", nargs="+", help="path to protein fasta files")
    parser.add_argument("-g", "--gff3", nargs="+", help="path to gff3 files")


def upset_args(parser):
    """ add orthoupset related args to subparser parser
    """
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument("-s", "--samples", nargs="+", help="the samples of interest")
    parser.add_argument("-n", "--names", nargs="*", help="the names to use for the samples", default=None)
    parser.add_argument("-t", "--title", default="Shared Orthogroup Sizes", help="the title to use")
    parser.add_argument("-o", "--outpath", default="orthoupset.svg", help="output image path")
    parser.add_argument("-c", "--columns", default=20, type=int, help="the maximum number of intersections/columns to plot")


def comps_args(parser):
    """ add orthocomps related args to subparser parser
    """
    def parse_comp(s):
        sets = s.split(":")
        if(len(sets) != 2):
            raise ValueError("Comparisons must specify exactly two sets. (only one ':')")
        s1, s2 = [s.split(",") for s in sets]
        if(len(set(s1) & set(s2)) != 0):
            raise ValueError("A label can't be in both sets in a comparison.")
        return (s1, s2)

    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument(
        "-s", "--sample",
        nargs="*",
        help="A specific sample for which each other sample should be compared against one at a time"
    )
    parser.add_argument(
        "-c", "--comparisons",
        nargs="*",
        type=parse_comp,
        help=(
            "the comparisons to be made. Should be formatted as "
            "'sample1,sample2:sample3,sample4' to compare the set"
            "sample1 + sample2 to the set sample3 + sample4."
            "Default behavior is to compare each sample to all other samples"
        )
    )

