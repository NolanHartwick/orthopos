import os
import sys
import json
import functools
import collections
import time
from typing import Generator, Tuple, Set, TextIO, List, Optional

import pandas
from statsmodels.stats.multitest import multipletests
from scipy.stats import fisher_exact
from scipy.special import expit
import pronto

data_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data")


def load_gff(f: str) -> pandas.DataFrame:
    """ Loads gff3 format file, partially and niavely parsing the attribute field

        >>> gff_path = os.path.join(data_path, "s1.gff3")
        >>> df = load_gff(gff_path)
        >>> df.shape
        (5, 12)
    """
    cols = "seqid source type start end score strand phase attr".split()
    df = pandas.read_csv(f, sep="\t", comment="#", header=None)
    df.columns = cols
    temp = df.attr.str.split(" *= *| *; *").apply(lambda l: dict(zip(l[::2], l[1::2])))
    df["Parent"] = temp.str["Parent"]
    df["ID"] = temp.str["ID"]
    df["attr_Name"] = temp.str["Name"]
    return df


def load_orthos(f: str) -> pandas.DataFrame:
    """ loads an orthogroups.tsv file from orthofinder

        >>> df = load_orthos(os.path.join(data_path, "example.orthogroups.tsv"))
        >>> df.shape
        (11, 3)
    """
    df = pandas.read_csv(f, sep="\t", index_col=0, dtype=str)
    df = df.stack().str.split(", *").reset_index()
    df_iter = zip(df.iloc[:, 0], df.iloc[:, 1], df.iloc[:, 2])
    ret = pandas.DataFrame(
        [[a, s, g] for a, s, gs in df_iter for g in gs],
        columns=["Orthogroup", "Sample", "Protein"]
    )
    return ret


def load_gos(fpath: str) -> pandas.DataFrame:
    """ parses a two column file containing go annotations

        >>> df = load_gos(os.path.join(data_path, "gos.s1.tsv"))
        >>> df.shape
        (7, 2)
    """ 
    df = pandas.read_csv(fpath, sep="\t", header=None, names=["gene", "gos"])
    df = df.dropna()
    mapping = df.groupby('gene').gos.apply(",".join).str.split(",")
    return pandas.DataFrame(
        [
            (g, go)
            for g, gos in mapping.items()
            for go in gos
        ],
        columns=["Protein", "GO"]
    )


def fast_fisher_df(df: pandas.DataFrame, cols: List[str]) -> pandas.DataFrame:
    """ Given a dataframe with three columns (genes, cat1, cat2) do fisher tests to compute
        overrepresentation between cat1 and cat2 given columnames from cols identifying which
        three columns to use. Returns dataframe in 5 columns: (lev1, lev2, pval, ratio, fdr)

        >>> og_path = os.path.join(data_path, "example.orthogroups.tsv")
        >>> orthos = load_orthos(og_path)
        >>> ret = fast_fisher_df(orthos, ["Protein", "Orthogroup", "Sample"])
        >>> correct_results_path = os.path.join(data_path, "fast_fisher.test.result.tsv")
        >>> res = pandas.read_csv(correct_results_path, sep="\\t", index_col=[0, 1])
        >>> round4 = lambda x: round(x, 4)
        >>> (res.applymap(round4) == ret.applymap(round4)).all().all()
        True
    """
    gid, lev1, lev2 = cols
    tot_genes = len(df[gid].unique())
    genes_per_lev1 = df.groupby(lev1)[gid].unique().apply(len)
    genes_per_lev2 = df.groupby(lev2)[gid].unique().apply(len)
    genes_per_both = df.groupby([lev1, lev2])[gid].unique().apply(len)
    counts = pandas.DataFrame({
        "lev1" : genes_per_both.index.get_level_values(0),
        "lev2" : genes_per_both.index.get_level_values(1),
        "both_count": genes_per_both.values,
        "lev1_count": genes_per_lev1.loc[genes_per_both.index.get_level_values(0)].values,
        "lev2_count": genes_per_lev2.loc[genes_per_both.index.get_level_values(1)].values,
    }).set_index(["lev1", "lev2"])
    counts["fisher_input"] = [
        ((vals[0], vals[2] - vals[0]), (vals[1] - vals[0], tot_genes - vals[2] - vals[1] + vals[0]))
        for vals in zip(counts.both_count, counts.lev1_count, counts.lev2_count)
    ]
    greater_fisher = functools.partial(fisher_exact, alternative="greater")
    counts["fisher_result"] = counts.fisher_input.apply(greater_fisher)
    counts["pval"] = counts["fisher_result"].str[1]
    counts["ratio"] = counts["fisher_result"].str[0]
    counts["FDR"] = multipletests(counts.pval, method="fdr_bh", alpha=0.05)[1]
    counts.index.names = [lev1, lev2]
    return counts[["pval", "ratio", "FDR"]]


def orthogos(
    orthogroups_path: str,
    go_pairs: List[Tuple[str, str]],
    weights: Tuple[int, int, int],
    obo: str,
    filter: int
) -> None:
    """ Cross reference go annotations with orthogroups to identify go
        terms overrepresented in each orthogroup. This is a psuedo entry
        point

        >>> og_path = os.path.join(data_path, "example.orthogroups.tsv")
        >>> s1_gos = os.path.join(data_path, "example.gos.s1.tsv")
        >>> s2_gos = os.path.join(data_path, "example.gos.s2.tsv")
        >>> obo_path = os.path.join(data_path, "go-basic.obo")
        >>> # orthogos(og_path, [("s1", s1_gos), ("s2", s2_gos)], (1, 1, 1), obo_path)
    """
    start = time.time()
    # process inputs
    orthos = load_orthos(orthogroups_path)
    all_samples = orthos.Sample.unique()
    missing_labels = [l for l, _ in go_pairs if l not in all_samples]
    if(len(missing_labels) != 0):
        raise ValueError(f"The following labels don't match provided orthogroups: {missing_labels}")
    go_maps = {k: load_gos(f) for k, f in go_pairs}
    go_maps = pandas.concat(go_maps.values(), keys=go_maps.keys(), names=["Sample"])
    go_maps = go_maps.reset_index()[["Sample", "Protein", "GO"]]
    all_data = pandas.merge(orthos, go_maps, on=["Sample", "Protein"], how="outer")
    all_data = all_data.dropna()
    all_data["GID"] = [(s, p) for s, p in zip(all_data.Sample, all_data.Protein)]
    sys.stderr.write(f"Data Loaded : {time.time() - start}\n")
    # do fisher tests
    results = fast_fisher_df(all_data, cols=["GID", "Orthogroup", "GO"])
    sys.stderr.write(f"Tests Finished : {time.time() - start}\n")
    # load go data
    godata = pronto.Ontology(obo)
    all_gos = set(results.index.get_level_values(1))
    go_to_descendants = {
        go: {g.id for g in godata[go].subclasses(with_self=False)} & all_gos
        if(go in godata) else {}
        for go in all_gos
    }
    to_process = list(sorted(go_to_descendants, key=lambda g: len(go_to_descendants[g])))
    pw, fcw, cw = weights
    prescore = (1-results.pval)**pw * expit(results.ratio)**fcw 
    prescore = prescore.reorder_levels([1, 0])
    scores = {go: {} for go in to_process}
    for go in to_process:
        descendants = go_to_descendants[go]
        for og, ps in prescore[go].items():
            if(len(descendants) == 0):
                desc_score = 0
            else:
                desc_score = max(scores.get(g, {}).get(og, 0) for g in descendants)
            scores[go][og] = ps * (1 - desc_score)**cw
    results["score"] = pandas.Series({
        (og, go): s for go, ogs in scores.items() for og, s in ogs.items()
    })
    results["description"] = [
        godata[go].name if(go in godata) else None
        for go in results.index.get_level_values(1)
    ]
    if(filter > 0):
        results = results.groupby(["Orthogroup"]).apply(
            lambda df: df.sort_values("score", ascending=False).head(filter).droplevel(0)
        )
    sys.stderr.write(f"Finished : {time.time() - start}\n")
    results.to_csv(sys.stdout, sep="\t")
    
    

def orthocomps(
    orthogroups_path:str,
    comp_lists: Optional[List[Tuple[List[str], List[str]]]],
    samps: Optional[List[str]]
) -> None:
    """ Perform a set of comparisons identifying enlarged orthogroups

        >>> og_path = os.path.join(data_path, "example.orthogroups.tsv")
        >>> # orthocomps(og_path, [("s1", "s2")], None)
    """
    orthos = load_orthos(orthogroups_path)
    all_samps = orthos.Sample.unique()
    if(comp_lists is None and samps is None):
        comp_lists = [([l], [a for a in all_samps if a != l]) for l in all_samps]
    if(comp_lists is None):
        comp_lists = []
    if(samps is not None):
        for samp in samps:
            comp_lists += [([samp], [l]) for l in all_samps if l != samp]
            comp_lists += [([l], [samp]) for l in all_samps if l != samp]
    comps = set((tuple(s1), tuple(s2)) for s1, s2 in comp_lists)
    missing_labels = set(l for c in comps for s in c for l in s if l not in all_samps)
    if(len(missing_labels) != 0):
        raise ValueError(f"The following labels don't match provided orthogroups: {missing_labels}")
    compnames = [f"{','.join(s1)}:{','.join(s2)}" for s1, s2 in comps]
    for comp, name in zip(comps, compnames):
        compmap = {l: f"set_{i}" for i, s in enumerate(comp) for l in s}
        orthos[name] = orthos.Sample.apply(compmap.get)
    results = [
        fast_fisher_df(
            orthos.dropna(subset=[name]),
            cols=["Protein", name, "Orthogroup"]
        ).loc["set_0"]
        for name in compnames
    ]
    results_df = pandas.concat(results, axis=0, keys=compnames)
    results_df.to_csv(sys.stdout, sep="\t")
