This repo contains a pip installable package meant to work with orthofinder output. It contains 4 principle tools designed to permit:
1. pos: integrating positional (gff3) information into orthogroups
2. comp: identifying orthogroups that are enlarged in some sample (set of samples)
3. gos: finding statistical associations between functional annotations and orthogroups
4. upset: visualize your orthofinder results using an upset plot

The simplest way to install is with pip. Command should be:

```
pip install git+https://gitlab.com/NolanHartwick/orthopos.git
```

Alternatively, if you want to use conda to manage the dependencies and installation, just use the 'requirements.yml' file to build a new env which will have ortho_analysis installed into. Commands should be...

```
wget https://gitlab.com/NolanHartwick/orthopos/-/raw/master/requirements.yml
conda env create -n ortho -f requirements.yml
conda activate ortho
```

### pos

This is a simple tool meant to add positional information to orthofinder output. This script accepts "Orthogroups.tsv" and gff3 files. It will then cross reference your orthogroups with your gff3 file and produce a summary table with the merged information.

```
ortho_analysis pos /your/orthofinder/output/Orthogroups.tsv -g /your/gff3/files/*.gff3 > Orthogroups.pos.tsv
```

### comp

This tool is meant to identify orthogroups that are enriched in some set of samples of interest relative to some other set. This script accepts "Orthogroups.tsv" and descriptions of what comparisons should be performed. These descriptions can come from either specificng a direct comparison between two sets of samples using the "--comparison" option, or by simply specifying a sample or set of samples using the "--sample" option. If you use the "sample" option, each sample you identified will be compared against all other samples in your "orthogroups.tsv" one at a time. 

```
ortho_analysis comp /your/orthofinder/output/Orthogroups.tsv -c sample1:sample2,sample3  > specific.comparison.results
ortho_analysis comp /your/orthofinder/output/Orthogroups.tsv -s sample3  > sample3.comparison.results
```

The first command would perform a single comparison between sample 1 and the combination of sample 2 and 3. The second command will perform N-1 comparisons where N is the number of samples in your "orthogroups.tsv" file. In either case, results are written to stdout


### upset

This tool is meant to generate 'upset' plots representing a set of samples in some "Orthogroups.tsv" file. It expects the orthogroups file, a list of sample IDs that should match columns in the file, and can optionially be provided: a list of names to be used in place of the IDs in the plot, an output path controlling where plot will be written, and a plot title. 

```
ortho_analysis upset /your/orthofinder/output/Orthogroups.tsv -o output.png -s sample1 sample2 sample3 -n name1 name2 name3 -t my_title
```

This will generate a file named "output.png" representing the samples of interest. The y axis in the plot represents a count of OG. For two samples to "share" an OG, they both must have at least one gene belonging to that OG.


### gos

This tool is designed to compute associations between go terms and orthogroups. It can be used to identify, for each orthogroup, what go terms are over represented among genes in that orthogroup. In order to run this script, you will need an "orthogroups.tsv" file and at least one two column tsv mapping gene ids from a single column of "orthogroups.tsv" to a comma seperated list of go terms. For example:

```
$cat at.gos.tsv
AT1G06180.1 GO:0000139,GO:0003674,GO:0003824,GO:0003836
AT1G08810.1 GO:0005575,GO:0005576,GO:0005622,GO:0005623,GO:0005737,GO:0005794,GO:0005795
AT1G08810.2 GO:0006464,GO:0006486
AT1G08810.2 GO:0006462,GO:0006487
```

Note that if a gene id appears in multiple rows, associated go terms will be merged automatically before advancing. Once you have your "orthogroups.tsv" and your "gos.tsv" file, you can invoke the script using a command like

```
ortho_analysis gos orthogroups.tsv arabidopsis=at.gos.tsv
```

Note that "arabidopsis" is a key used to associate "at.gos.tsv" with a specific column in "orthogroups.tsv" and must be exactly equal to a column found in "orthogroups.tsv". You can provide go terms for many organisms by adding more "key=gos" pairs to your commmand:

```
ortho_analysis gos orthogroups.tsv arabidopsis=at.gos.tsv osativa=osa.gos.tsv quercus=qs.gos.tsv
```

By default only the best three go terms for each orthogroup are reported in the output. You can change this behavior using the "--filter" option. The "best" go terms are the go terms with the maximum score. Score is defined as...

```
(1 - p_value) ** W1 * logit(fold_change) ** W2 * (1 - max(score(c) for c in children)) ** W3
```

...where W1, W2, and W3 are weights specified using the "--weights" option and default to (1, 1, 1). In english, a go term has a high score if it is very significant (low p value), shows a strong effect size (high fold change), and no "more specific" go term has a high score. This prevents a high scoring go term and its parent from showing up frequently and makes the reported go terms more informative. 

