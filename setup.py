from setuptools import setup

setup(
    name="orthopos",
    version="0.2.0",
    packages=["ortho_analysis"],
    python_requires=">=3.7",
    install_requires=[
        "pandas",
        "scipy",
        "statsmodels",
        "upsetplot",
        "pronto",
        "matplotlib"
    ],
    entry_points={
        "console_scripts": [
            "ortho_analysis = ortho_analysis.__main__:main"
        ]
    },
    package_data={
        "ortho_analysis": ["go-basic.obo"]
    }
)